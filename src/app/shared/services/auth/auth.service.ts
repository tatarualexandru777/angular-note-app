import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import {from, Observable, tap} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthService {
  firebaseUser: firebase.User;
  $user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, private router: Router, private route: ActivatedRoute) {
    this.$user = firebaseAuth.authState; //async pipe to automatically unsusbscribe from the observable when it's destroyed
    this.firebaseAuth.authState
      .pipe(
        tap((user: firebase.User) => {
          this.firebaseUser = user;
        })
      )
      .subscribe((user: firebase.User) => {
        if (user) {
          localStorage.setItem('user', JSON.stringify(user));
          JSON.parse(localStorage.getItem('user'));
        } else {
          localStorage.setItem('user', null);
          JSON.parse(localStorage.getItem('user'));
        }
      });
  }

  signUp(email: string, username: string, password: string) {
    return this.firebaseAuth.createUserWithEmailAndPassword(email, password);
  }

  loginWithGoogle() {
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/notes';
    localStorage.setItem('returnUrl', returnUrl);
    this.firebaseAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  loginWithEmail(email: string, password: string) {
    return from(this.firebaseAuth.signInWithEmailAndPassword(email, password));
  }

  logout() {
    this.firebaseAuth.signOut();
    localStorage.setItem('user', null);
    this.router.navigate(['/log-in']);
  }
}
