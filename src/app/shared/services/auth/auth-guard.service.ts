import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';
import {map, Observable} from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.auth.$user.pipe(
      map(user => {
        if (user) {
          return true;
        }
        this.router.navigate(['/log-in'], {queryParams: {returnUrl: state.url}});
        return false;
      })
    );
  }
}
