import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup} from '@angular/forms';
import {Note} from '../../../notes/note.interface';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  dialogForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: Note) {}

  ngOnInit(): void {
    this.dialogForm = new FormGroup({
      title: new FormControl(''),
      content: new FormControl(''),
      startDate: new FormControl(''),
      endDate: new FormControl('')
    });
    this.dialogForm.patchValue(this.data);
  }

  closeDialog(save?: boolean) {
    if (save) {
      this.dialogRef.close(this.dialogForm.value);
    } else {
      this.dialogRef.close();
    }
  }
}
