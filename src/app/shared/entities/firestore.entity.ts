import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/compat/firestore';
import {from, map, Observable, of} from 'rxjs';
import firebase from 'firebase/compat';
import WhereFilterOp = firebase.firestore.WhereFilterOp;

@Injectable({providedIn: 'root'})
export class FirestoreEntity {
  constructor(private firestore: AngularFirestore) {}

  getCollection<T>(collectionName: string): Observable<T> {
    const collectionObservable = this.firestore.collection(collectionName).get() as unknown as Observable<any>;
    return collectionObservable.pipe(
      map(items =>
        items.docs.map(doc => {
          const newDoc = doc.data();
          newDoc.id = doc.id;
          return newDoc as T;
        })
      )
    ) as Observable<T>;
  }

  getFilteredCollection<T>(collectionName: string, filter: {key: string; operator: WhereFilterOp; value: string | number}[]): Observable<T> {
    if (!filter?.length) {
      return of(null);
    }
    const collectionObservable = this.firestore
      .collection(collectionName, ref => {
        let query = ref.where(filter[0]?.key, filter[0]?.operator, filter[0]?.value);

        if (filter.length > 1) {
          query = query.where(filter[1]?.key || '', filter[1]?.operator || '==', filter[1]?.value || '');
        }
        return query;
      })
      .get() as unknown as Observable<any>;
    return collectionObservable.pipe(
      map(items =>
        items.docs.map(doc => {
          const newDoc = doc.data();
          newDoc.id = doc.id;
          return newDoc as T;
        })
      )
    ) as Observable<T>;
  }

  addDocument<T>(collectionName: string, documentData: T): Observable<T> {
    const collection = this.firestore.collection(collectionName);
    return from(collection.add(documentData)) as unknown as Observable<T>;
  }

  updateDocument<T>(collectionName: string, docId?: string, data?: T): Observable<T> {
    data = JSON.parse(JSON.stringify(data));
    const document = this.firestore.doc(collectionName + '/' + docId);
    if ((data as any).id) {
      delete (data as any).id;
    }
    return from(document.update(data)) as unknown as Observable<T>;
  }

  deleteDocument<T>(collectionName: string, docId: string): Observable<T> {
    const document = this.firestore.doc(collectionName + '/' + docId);
    return from(document.delete()) as unknown as Observable<T>;
  }
}
