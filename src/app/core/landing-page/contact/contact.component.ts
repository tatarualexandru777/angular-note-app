import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;

  constructor() {}

  submitForm() {
    window.open(
      'mailto:tatarualexandru777@gmail.com?subject=' +
        'From ' +
        this.contactForm.value.firstName +
        ' ' +
        this.contactForm.value.lastName +
        '&body=' +
        this.contactForm.value.textBody
    );
    console.log(this.contactForm.value);
  }

  ngOnInit(): void {
    this.contactForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      textBody: new FormControl('', [Validators.required])
    });
  }
}
