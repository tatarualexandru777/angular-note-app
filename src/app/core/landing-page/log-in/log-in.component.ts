import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;

  constructor(public auth: AuthService) {}

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  // loginWithEmail() {
  //   this.auth.loginWithEmail(username, password);
  // }

  loginWithGoogle() {
    this.auth.loginWithGoogle();
  }

  loginWithEmail() {
    if (!this.loginForm.valid) {
      return;
    }

    const {email: email} = this.loginForm.value;
    const {password: password} = this.loginForm.value;
    this.auth.loginWithEmail(email, password).subscribe(
      () => console.log('success'),
      () => this.loginForm.get('password').setErrors({incorrect: true})
    );
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
  }
}
