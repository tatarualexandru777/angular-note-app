import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  hide = true;
  signUpForm: FormGroup;

  constructor(public auth: AuthService) {}

  get email() {
    return this.signUpForm.get('email');
  }

  get username() {
    return this.signUpForm.get('username');
  }

  get password() {
    return this.signUpForm.get('password');
  }

  get confirmPassword() {
    return this.signUpForm.get('confirmPassword');
  }

  ngOnInit(): void {
    this.signUpForm = new FormGroup(
      {
        email: new FormControl('', [Validators.email, Validators.required]),
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        confirmPassword: new FormControl('', Validators.required)
      },
      {validators: this.passwordsMatchValidator()}
    );
  }

  passwordsMatchValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const password = control.get('password')?.value;
      const confirmPassword = control.get('confirmPassword')?.value;

      if (password && confirmPassword && password !== confirmPassword) {
        return {
          passwordsDontMatch: true
        };
      }
      return null;
    };
  }

  signUp() {
    if (!this.signUpForm.valid) {
      return;
    }

    const {email: email} = this.signUpForm.value;
    const {username: username} = this.signUpForm.value;
    const {password: password} = this.signUpForm.value;

    this.auth.signUp(email, username, password);
  }
}
