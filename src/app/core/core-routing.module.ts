import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FeaturesComponent} from './landing-page/features/features.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {LogInComponent} from './landing-page/log-in/log-in.component';
import {SignUpComponent} from './landing-page/sign-up/sign-up.component';
import {ContactComponent} from './landing-page/contact/contact.component';

const coreRoutes: Routes = [
  {
    path: 'landing-page',
    component: LandingPageComponent
    // children: [
    //   {
    //     path: 'features',
    //     component: FeaturesComponent
    //   },
    //   {
    //     path: 'log-in',
    //     component: LogInComponent
    //   },
    //   {
    //     path: 'sign-up',
    //     component: SignUpComponent
    //   },
    //   {
    //     path: 'contact',
    //     component: ContactComponent
    //   }
    // ]
  },
  {
    path: 'features',
    component: FeaturesComponent
  },
  {
    path: 'log-in',
    component: LogInComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(coreRoutes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
