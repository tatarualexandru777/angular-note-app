import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {FirestoreEntity} from '../shared/entities/firestore.entity';
import {Goal} from './goal.interface';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {AuthService} from '../shared/services/auth/auth.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {
  addGoalForm: FormGroup;
  goals: Goal[] = [];

  constructor(private firestoreEntity: FirestoreEntity, private authService: AuthService, private dialog: MatDialog) {}

  get content() {
    return this.addGoalForm.get('content') as FormArray;
  }

  get isCompleted() {
    return this.addGoalForm.get('isCompleted') as FormArray;
  }

  ngOnInit(): void {
    this.getGoals();
    this.addGoalForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      content: new FormArray([new FormControl('', [Validators.required, Validators.maxLength(40)])])
    });
  }

  addGoal() {
    const goal = JSON.parse(JSON.stringify(this.addGoalForm.value));
    goal.userId = this.authService.firebaseUser.uid;
    goal.content = goal.content.map(step => {
      return {
        value: step,
        isChecked: false
      };
    });
    this.firestoreEntity.addDocument<Goal>('goals', goal).subscribe(newGoal => {
      goal.id = newGoal.id;
      this.goals.push(goal);
      this.addGoalForm.reset();
    });
  }

  editGoal(event: any, contentElement: {value: string}, goal: Goal) {
    contentElement.value = event.target.value;
    this.firestoreEntity.updateDocument<Goal>('goals', goal.id, goal).subscribe();
  }

  deleteGoal(id: string) {
    this.firestoreEntity.deleteDocument<Goal>('goals', id).subscribe(() => {
      const goalIndex = this.goals.findIndex(goal => goal.id === id);
      this.goals.splice(goalIndex, 1);
    });
  }

  getGoals() {
    this.firestoreEntity
      .getFilteredCollection<Goal[]>('goals', [
        {
          key: 'userId',
          operator: '==',
          value: this.authService.firebaseUser.uid
        }
      ])
      .subscribe(goals => {
        this.goals = goals;
      });
  }

  addInputControl() {
    this.content.push(new FormControl(''));
  }

  removeInputControl(index: number) {
    this.content.removeAt(index);
    this.isCompleted.removeAt(index);
  }

  onGoalContentChanged(event: MatCheckboxChange, contentElement: {isChecked: boolean}, goal: Goal) {
    contentElement.isChecked = event.checked;
    this.firestoreEntity.updateDocument<Goal>('goals', goal.id, goal).subscribe();
  }
}
