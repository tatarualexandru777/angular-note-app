import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GoalsComponent} from './goals.component';
import {AuthGuardService} from '../shared/services/auth/auth-guard.service';

const goalsRoutes: Routes = [
  {
    path: 'goals',
    component: GoalsComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(goalsRoutes)],
  exports: [RouterModule]
})
export class GoalsRoutingModule {}
